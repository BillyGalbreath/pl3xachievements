package net.pl3x.bukkit.achievements;

import net.minecraft.server.v1_11_R1.ServerStatisticManager;
import net.minecraft.server.v1_11_R1.Statistic;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class BiomeHelper {
    public static final List<String> EXPLORE_BIOMES = new ArrayList<>();

    static {
        EXPLORE_BIOMES.add("Beach");
        EXPLORE_BIOMES.add("Birch Forest");
        EXPLORE_BIOMES.add("Birch Forest Hills");
        EXPLORE_BIOMES.add("Cold Beach");
        EXPLORE_BIOMES.add("Cold Taiga");
        EXPLORE_BIOMES.add("Cold Taiga Hills");
        EXPLORE_BIOMES.add("Deep Ocean");
        EXPLORE_BIOMES.add("Desert");
        EXPLORE_BIOMES.add("DesertHills");
        EXPLORE_BIOMES.add("Extreme Hills");
        EXPLORE_BIOMES.add("Extreme Hills+");
        EXPLORE_BIOMES.add("Forest");
        EXPLORE_BIOMES.add("ForestHills");
        EXPLORE_BIOMES.add("FrozenRiver");
        EXPLORE_BIOMES.add("Ice Mountains");
        EXPLORE_BIOMES.add("Ice Plains");
        EXPLORE_BIOMES.add("Jungle");
        EXPLORE_BIOMES.add("JungleEdge");
        EXPLORE_BIOMES.add("JungleHills");
        EXPLORE_BIOMES.add("Mega Taiga");
        EXPLORE_BIOMES.add("Mega Taiga Hills");
        EXPLORE_BIOMES.add("Mesa");
        EXPLORE_BIOMES.add("Mesa Plateau");
        EXPLORE_BIOMES.add("Mesa Plateau F");
        EXPLORE_BIOMES.add("MushroomIsland");
        EXPLORE_BIOMES.add("MushroomIslandShore");
        EXPLORE_BIOMES.add("Ocean");
        EXPLORE_BIOMES.add("Plains");
        EXPLORE_BIOMES.add("River");
        EXPLORE_BIOMES.add("Roofed Forest");
        EXPLORE_BIOMES.add("Savanna");
        EXPLORE_BIOMES.add("Savanna Plateau");
        EXPLORE_BIOMES.add("Stone Beach");
        EXPLORE_BIOMES.add("Swampland");
        EXPLORE_BIOMES.add("Taiga");
        EXPLORE_BIOMES.add("TaigaHills");
    }

    public static List<String> getVisitedBiomes(Player player) {
        ServerStatisticManager manager = ((CraftPlayer) player).getHandle().getStatisticManager();

        Object obj = null;
        try {
            Field fieldE = manager.getClass().getDeclaredField("e");
            fieldE.setAccessible(true);
            obj = fieldE.get(manager);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        if (obj == null || !(obj instanceof Set)) {
            return null;
        }

        for (Object item : (Set) obj) {
            if (!(item instanceof Statistic)) {
                continue;
            }

            Statistic stat = (Statistic) item;

            if (!stat.name.equals("achievement.exploreAllBiomes")) {
                continue;
            }

            return Arrays.asList(manager.b(stat).toString().replace("[", "").replace("]", "").split(", "));
        }

        return new ArrayList<>();
    }
}
