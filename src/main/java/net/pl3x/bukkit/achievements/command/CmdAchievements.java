package net.pl3x.bukkit.achievements.command;

import net.pl3x.bukkit.achievements.Pl3xAchievements;
import net.pl3x.bukkit.achievements.configuration.Config;
import net.pl3x.bukkit.achievements.configuration.Lang;
import net.pl3x.bukkit.achievements.task.CheckAchievements;
import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CmdAchievements implements TabExecutor {
    private final Pl3xAchievements plugin;

    public CmdAchievements(Pl3xAchievements plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> results = new ArrayList<>();
        if (args.length == 1) {
            results.addAll(Stream.of("reload", "give", "take")
                    .filter(name -> name.startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList()));
        }
        if (args.length == 2) {
            results.addAll(Bukkit.getOnlinePlayers()
                    .stream().filter(player -> player.getName().toLowerCase().startsWith(args[1].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList()));
        }
        if (args.length == 3) {
            String arg = args[2].toLowerCase();
            for (Achievement achievement : Achievement.values()) {
                String name = achievement.name().toLowerCase().replace("_", "-");
                if (name.startsWith(arg)) {
                    results.add(name);
                }
            }
            if ("all".startsWith(arg)) {
                results.add("all");
            }
        }
        return results;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            Lang.send(sender, Lang.VERSION
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()));
            return true;
        }

        if (args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("command.achievements.reload")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }

            Config.reload();
            Lang.reload();

            Lang.send(sender, Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()));
            return true;
        }

        if (args.length == 1) {
            Lang.send(sender, Lang.MUST_SPECIFY_PLAYER);
            return true;
        }

        if (args.length == 2) {
            Lang.send(sender, Lang.MUST_SPECIFY_ACHIEVEMENT);
            return true;
        }

        OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
        if (target == null) {
            Lang.send(sender, Lang.PLAYER_NOT_FOUND);
            return true;
        }
        Player targetPlayer = target.getPlayer();
        if (targetPlayer == null) {
            Lang.send(sender, Lang.PLAYER_NOT_ONLINE);
            return true;
        }

        String name = args[2].trim().toUpperCase().replace("-", "_");
        List<Achievement> achievements = new ArrayList<>();
        try {
            achievements.add(Achievement.valueOf(name));
        } catch (IllegalArgumentException ignore) {
            if (name.equals("ALL")) {
                Collections.addAll(achievements, Achievement.values());
            }
        }

        if (achievements.isEmpty()) {
            Lang.send(sender, Lang.ACHIEVEMENT_NOT_FOUND);
            return true;
        }

        if (args[0].equalsIgnoreCase("give")) {
            if (!sender.hasPermission("command.achievements.give")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            achievements.forEach(targetPlayer::awardAchievement);
            Lang.send(sender, Lang.ACHIEVEMENT_GIVEN);
            new CheckAchievements(plugin, targetPlayer).runTaskLater(Pl3xAchievements.getPlugin(), 20);
            return true;
        }

        if (args[0].equalsIgnoreCase("take")) {
            if (!sender.hasPermission("command.achievements.take")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            achievements.forEach(targetPlayer::removeAchievement);
            Lang.send(sender, Lang.ACHIEVEMENT_TAKEN);
            return true;
        }

        Lang.send(sender, Lang.UNKNOWN_COMMAND);
        return true;
    }
}
