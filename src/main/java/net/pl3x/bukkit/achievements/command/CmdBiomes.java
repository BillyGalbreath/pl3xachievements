package net.pl3x.bukkit.achievements.command;

import net.pl3x.bukkit.achievements.BiomeHelper;
import net.pl3x.bukkit.achievements.configuration.Lang;
import org.bukkit.Achievement;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class CmdBiomes implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.listbiomes")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        Player player = (Player) sender;
        List<String> visitedBiomes = BiomeHelper.getVisitedBiomes(player);
        String biomes = "";

        for (String biome : BiomeHelper.EXPLORE_BIOMES) {
            biomes += !biomes.isEmpty() ? Lang.BIOMES_LIST_DELIMITER : "";
            if (player.hasAchievement(Achievement.EXPLORE_ALL_BIOMES) || (visitedBiomes != null && visitedBiomes.contains(biome))) {
                biomes += Lang.BIOMES_LIST_EARNED + biome;
            } else {
                biomes += Lang.BIOMES_LIST_MISSING + biome;
            }
        }

        Lang.send(sender, Lang.BIOMES_LIST);
        Lang.send(sender, biomes);
        return true;
    }
}
