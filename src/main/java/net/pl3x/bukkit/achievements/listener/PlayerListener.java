package net.pl3x.bukkit.achievements.listener;

import net.pl3x.bukkit.achievements.Pl3xAchievements;
import net.pl3x.bukkit.achievements.task.CheckAchievements;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {
    private final Pl3xAchievements plugin;

    public PlayerListener(Pl3xAchievements plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        new CheckAchievements(plugin, event.getPlayer()).runTaskLater(plugin, 20);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerAchievementAwarded(PlayerAchievementAwardedEvent event) {
        new CheckAchievements(plugin, event.getPlayer()).runTaskLater(plugin, 20);
    }
}
