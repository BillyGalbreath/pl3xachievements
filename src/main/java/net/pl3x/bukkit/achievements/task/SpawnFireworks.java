package net.pl3x.bukkit.achievements.task;

import net.pl3x.bukkit.achievements.Pl3xAchievements;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

class SpawnFireworks extends BukkitRunnable {
    private final Location base;

    public SpawnFireworks(Location base) {
        this.base = base;
    }

    @Override
    public void run() {
        Pl3xAchievements plugin = Pl3xAchievements.getPlugin();
        new SpawnFirework(base.clone().add(3, 0, 3)).runTask(plugin);
        new SpawnFirework(base.clone().add(-3, 0, -3)).runTaskLater(plugin, 20);
        new SpawnFirework(base.clone().add(1, 0, -3)).runTaskLater(plugin, 40);
        new SpawnFirework(base.clone().add(-3, 0, 3)).runTaskLater(plugin, 60);
        new SpawnFirework(base.clone().add(3, 0, 1)).runTaskLater(plugin, 80);
        new SpawnFirework(base.clone().add(1, 0, -1)).runTaskLater(plugin, 100);
    }
}
