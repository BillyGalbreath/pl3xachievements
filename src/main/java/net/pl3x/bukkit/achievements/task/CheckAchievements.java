package net.pl3x.bukkit.achievements.task;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.pl3x.bukkit.achievements.Logger;
import net.pl3x.bukkit.achievements.Pl3xAchievements;
import net.pl3x.bukkit.achievements.configuration.Config;
import net.pl3x.bukkit.achievements.configuration.Lang;
import net.pl3x.bukkit.achievements.hook.VaultHook;
import net.pl3x.bukkit.chatapi.ComponentSender;
import net.pl3x.bukkit.titleapi.Title;
import net.pl3x.bukkit.titleapi.api.TitleType;
import org.bukkit.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class CheckAchievements extends BukkitRunnable {
    private final Pl3xAchievements plugin;
    private final Player player;

    public CheckAchievements(Pl3xAchievements plugin, Player player) {
        this.plugin = plugin;
        this.player = player;
    }

    @Override
    public void run() {
        Logger.debug("Checking achievements for " + player.getName());

        String group = Config.GROUP_NAME;
        if (VaultHook.playerInGroup(player, group)) {
            Logger.debug(player.getName() + " is already in " + group + " group.");
            return; // already in group
        }

        // check if player has all achievements
        boolean missing = false;
        for (Achievement achievement : Achievement.values()) {
            if (!player.hasAchievement(achievement)) {
                Logger.debug(player.getName() + " is missing achievement " + achievement.name());
                missing = true;
            } else {
                Logger.debug(player.getName() + " has achievement " + achievement.name());
            }
        }

        if (missing) {
            return;
        }

        if (!VaultHook.playerAddGroup(player, group)) {
            Logger.error("Something went wrong adding " + player.getName() + " to group " + group);
            return;
        }

        Lang.broadcast(new HashSet<>(Collections.singletonList(player)), Lang.CHATGLOBAL
                .replace("{player}", player.getName())
                .replace("{group}", group));

        Lang.send(player, Lang.CHATACHIEVER
                .replace("{player}", player.getName())
                .replace("{group}", group));

        // announce to all!
        new Title(Config.TITLE_TIME_FADE_IN,
                Config.TITLE_TIME_STAY,
                Config.TITLE_TIME_FADE_OUT)
                .broadcast();
        new Title(TitleType.TITLE, Lang.TITLE
                .replace("{player}", player.getName())
                .replace("{group}", group))
                .broadcast();
        new Title(TitleType.SUBTITLE, Lang.SUBTITLE
                .replace("{player}", player.getName())
                .replace("{group}", group))
                .broadcast();
        BaseComponent[] actionbar = TextComponent.fromLegacyText(Lang.ACTIONBAR
                .replace("{player}", player.getName())
                .replace("{group}", group));
        for (Player online : Bukkit.getOnlinePlayers()) {
            ComponentSender.sendMessage(online, ChatMessageType.ACTION_BAR, actionbar);
        }

        // fireworks \o/
        new SpawnFireworks(player.getLocation()).runTask(plugin);

        // special commands
        List<String> commands = Config.COMMANDS;
        if (commands == null || commands.isEmpty()) {
            return;
        }
        for (String command : commands) {
            if (command == null || command.isEmpty()) {
                continue;
            }

            command = command.replace("{player}", player.getName());

            Logger.debug("Running command: " + command);
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
        }
    }
}
