package net.pl3x.bukkit.achievements.task;

import net.pl3x.bukkit.achievements.Pl3xAchievements;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

class SpawnFirework extends BukkitRunnable {
    private final Location location;
    private Firework firework;
    private final Random random = new Random();

    public SpawnFirework(Location location) {
        this.location = location;
    }

    @Override
    public void run() {
        firework = location.getWorld().spawn(location, Firework.class);

        FireworkMeta meta = firework.getFireworkMeta();
        meta.addEffect(FireworkEffect.builder()
                .trail(getRandom(0, 1) == 1)
                .flicker(getRandom(0, 1) == 1)
                .withColor(Color.fromRGB(getRandom(0, 255), getRandom(0, 255), getRandom(0, 255)))
                .withFade(Color.fromRGB(getRandom(0, 255), getRandom(0, 255), getRandom(0, 255)))
                .with(FireworkEffect.Type.values()[getRandom(0, FireworkEffect.Type.values().length - 1)])
                .build());

        meta.setPower(1);

        firework.setFireworkMeta(meta);

        // wait at least 2 ticks then
        new BukkitRunnable() {
            @Override
            public void run() {
                firework.detonate();
            }
        }.runTaskLater(Pl3xAchievements.getPlugin(), getRandom(10, 40));
    }

    private int getRandom(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }
}
