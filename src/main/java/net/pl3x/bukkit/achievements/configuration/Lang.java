package net.pl3x.bukkit.achievements.configuration;

import net.pl3x.bukkit.achievements.Pl3xAchievements;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.Set;

public class Lang {
    public static String COMMAND_NO_PERMISSION = "&4You do not have permission for this command!";
    public static String PLAYER_COMMAND = "&4Player only command!";
    public static String UNKNOWN_COMMAND = "&4Unknown command!";
    public static String CHATACHIEVER = "&6&o===============================================\n&6&oCongratulations {player}!\n&6{player} has just earned the {group} title by mastering all achievements!\n&6Job well done! ^_^\n&6&o===============================================";
    public static String CHATGLOBAL = "&6&o===============================================\n&6&oAttention everybody!\n&6{player} has just earned the {group} title by mastering all achievements!\n&6Don't forget to congratulate them on a job well done! ^_^\n&6&o===============================================";
    public static String TITLE = "&9&lCongratulations!";
    public static String SUBTITLE = "&6&oTo {player}";
    public static String ACTIONBAR = "&b&o{player} has mastered all achievements!";
    public static String MUST_SPECIFY_PLAYER = "&4You must specify a player!";
    public static String PLAYER_NOT_FOUND = "&4Player not found!";
    public static String PLAYER_NOT_ONLINE = "&4Player not online!";
    public static String MUST_SPECIFY_ACHIEVEMENT = "&4You must specify an achievement!";
    public static String ACHIEVEMENT_NOT_FOUND = "&4Achievement not found!";
    public static String ACHIEVEMENT_GIVEN = "&dAchievements awarded!";
    public static String ACHIEVEMENT_TAKEN = "&dAchievements taken!";
    public static String BIOMES_LIST = "&dVisited Biomes List&e:";
    public static String BIOMES_LIST_DELIMITER = "&e, ";
    public static String BIOMES_LIST_MISSING = "&c";
    public static String BIOMES_LIST_EARNED = "&a";
    public static String VERSION = "&d{plugin} v{version}.";
    public static String RELOAD = "&d{plugin} v{version} reloaded.";

    public static void reload() {
        Pl3xAchievements plugin = Pl3xAchievements.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for this command!");
        PLAYER_COMMAND = config.getString("player-command", "&4Player only command!");
        UNKNOWN_COMMAND = config.getString("unknown-command", "&4Unknown command!");
        CHATACHIEVER = config.getString("chatachiever", "&6&o===============================================\n&6&oCongratulations {player}!\n&6{player} has just earned the {group} title by mastering all achievements!\n&6Job well done! ^_^\n&6&o===============================================");
        CHATGLOBAL = config.getString("chatglobal", "&6&o===============================================\n&6&oAttention everybody!\n&6{player} has just earned the {group} title by mastering all achievements!\n&6Don't forget to congratulate them on a job well done! ^_^\n&6&o===============================================");
        TITLE = config.getString("title", "&9&lCongratulations!");
        SUBTITLE = config.getString("subtitle", "&6&oTo {player}");
        ACTIONBAR = config.getString("actionbar", "&b&o{player} has mastered all achievements!");
        MUST_SPECIFY_PLAYER = config.getString("must-specify-player", "&4You must specify a player!");
        PLAYER_NOT_FOUND = config.getString("player-not-found", "&4Player not found!");
        PLAYER_NOT_ONLINE = config.getString("player-not-online", "&4Player not online!");
        MUST_SPECIFY_ACHIEVEMENT = config.getString("must-specify-achievement", "&4You must specify an achievement!");
        ACHIEVEMENT_NOT_FOUND = config.getString("achievement-not-found", "&4Achievement not found!");
        ACHIEVEMENT_GIVEN = config.getString("achievement-given", "&dAchievements awarded!");
        ACHIEVEMENT_TAKEN = config.getString("achievement-taken", "&dAchievements taken!");

        BIOMES_LIST = config.getString("biomes-list", "&dVisited Biomes List&e:");
        BIOMES_LIST_DELIMITER = config.getString("biomes-list-delimiter", "&e, ");
        BIOMES_LIST_MISSING = config.getString("biomes-list-missing", "&c");
        BIOMES_LIST_EARNED = config.getString("biomes-list-earned", "&a");

        VERSION = config.getString("version", "&d{plugin} v{version}.");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }

    public static void broadcast(Set<Player> ignore, String message) {
        Bukkit.getOnlinePlayers().stream()
                .filter(recipient -> !ignore.contains(recipient))
                .forEach(recipient -> send(recipient, message));
        send(Bukkit.getConsoleSender(), message); // always include console in broadcasts
    }
}
