package net.pl3x.bukkit.achievements.configuration;

import net.pl3x.bukkit.achievements.Pl3xAchievements;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static String GROUP_NAME = "AchievementSpecialist";
    public static int TITLE_TIME_FADE_IN = 10;
    public static int TITLE_TIME_STAY = 60;
    public static int TITLE_TIME_FADE_OUT = 20;
    public static List<String> COMMANDS = null;

    public static void reload() {
        Pl3xAchievements plugin = Pl3xAchievements.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        GROUP_NAME = config.getString("group-name", "AchievementSpecialist");
        TITLE_TIME_FADE_IN = config.getInt("title-time-fade-in", 10);
        TITLE_TIME_STAY = config.getInt("title-time-stay", 60);
        TITLE_TIME_FADE_OUT = config.getInt("title-time-fade-out", 20);
        COMMANDS = config.getStringList("commands");
    }
}
