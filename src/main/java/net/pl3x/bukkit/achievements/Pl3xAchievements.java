package net.pl3x.bukkit.achievements;

import net.pl3x.bukkit.achievements.command.CmdAchievements;
import net.pl3x.bukkit.achievements.command.CmdBiomes;
import net.pl3x.bukkit.achievements.configuration.Config;
import net.pl3x.bukkit.achievements.configuration.Lang;
import net.pl3x.bukkit.achievements.hook.VaultHook;
import net.pl3x.bukkit.achievements.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xAchievements extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        if (!(Bukkit.getPluginManager().isPluginEnabled("Vault") && VaultHook.setupPermissions())) {
            Logger.error("&4#&4#############################################################################&4#");
            Logger.error("&4#&c                Dependency plugin Vault was not found/loaded!                &4#");
            Logger.error("&4#&4                                                                             &4#");
            Logger.error("&4#&4     To prevent server crashes and other undesired behavior this plugin      &4#");
            Logger.error("&4#&4       is disabling itself from running. Please remove the plugin jar        &4#");
            Logger.error("&4#&4       from your plugins directory to free up the registered commands.       &4#");
            Logger.error("&4#&4#############################################################################&4#");
            return;
        }

        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

        getCommand("achievements").setExecutor(new CmdAchievements(this));
        getCommand("listbiomes").setExecutor(new CmdBiomes());

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                "&4" + getName() + " plugin is disabled. Please see console for more information."));
        return true;
    }

    public static Pl3xAchievements getPlugin() {
        return Pl3xAchievements.getPlugin(Pl3xAchievements.class);
    }
}
