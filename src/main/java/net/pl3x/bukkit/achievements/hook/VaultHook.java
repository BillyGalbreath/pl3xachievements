package net.pl3x.bukkit.achievements.hook;

import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultHook {
    private static Permission permission;

    public static boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServicesManager().getRegistration(Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return permission != null;
    }

    public static boolean playerInGroup(Player player, String group) {
        return permission.playerInGroup(player, group);
    }

    public static boolean playerAddGroup(Player player, String group) {
        return permission.playerAddGroup(player, group);
    }
}
